'use strict';


let showPassFirst = document.querySelector('.pass-hide-first');
let hidePassFirst = document.querySelector(".pass-show-first");
let firstInput = document.querySelector(".first");
let firstWrapper = document.querySelector('.wrapper-first');



function showPassFirstInput(){
    showPassFirst.style.display = "none";
    hidePassFirst.style.display = "block";
    firstInput.type = "text";
}

showPassFirst.addEventListener('click', showPassFirstInput);

function hidePassFirstInput(){
    showPassFirst.style.display = "block";
    hidePassFirst.style.display = "none";
    firstInput.type = "password";
}

hidePassFirst.addEventListener('click', hidePassFirstInput);


// SECOND INPUT 

let showPassSecond = document.querySelector('.pass-hide-second');
let hidePassSecond = document.querySelector(".pass-show-second");
let secondInput = document.querySelector(".second");


function showPassSecondInput(){
    showPassSecond.style.display = "none";
    hidePassSecond.style.display = "block";
    secondInput.type = "text";
}

showPassSecond.addEventListener('click', showPassSecondInput);

function hidePassSecondInput(){
    showPassSecond.style.display = "block";
    hidePassSecond.style.display = "none";
    secondInput.type = "password";
}

hidePassSecond.addEventListener('click', hidePassSecondInput);


// compare password
let passwordForm = document.querySelector(".password-form");
passwordForm.addEventListener('submit' , function comparePass(){
    let firstValue = firstInput.value;
    let secondValue = secondInput.value;

    if (firstValue===secondValue) alert ("WELCOME");
    else{
        let p = document.createElement('p');
        p.textContent = "Введіть правильний пароль";
        p.style.color = "red";
        p.style.margin = "0 0 0 0";

        let secondInput = document.querySelector(".second-wrapper")
        secondInput.append(p);
    }

})


